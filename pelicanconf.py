#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import datetime

AUTHOR = u'mujeebcpy'
SITENAME = u'ഇമ്മിണി ബെല്യേ കമ്പ്യൂട്ടിംഗ്'
SITEURL = ''
SLUGIFY_SOURCE = 'basename'

PATH = 'content'
OUTPUT_PATH = 'public'
THEME = 'theme/MinimalXY'
TIMEZONE = 'Asia/Kolkata'


# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

DEFAULT_PAGINATION = 5

# Theme customizations
MINIMALXY_CUSTOM_CSS = 'static/custom.css'
MINIMALXY_FAVICON = 'favicon.ico'
MINIMALXY_START_YEAR = 2017
#MINIMALXY_CURRENT_YEAR = date.today().year

# Author
AUTHOR_INTRO = u'Hello! I’m Mujeeb Rahman K.'
AUTHOR_DESCRIPTION = u'Im a Foss Enthusiast, Tech Youtuber and co founder of alphafork Technologies.'
AUTHOR_AVATAR = 'https://secure.gravatar.com/avatar/af2a8bc34d5f1a49b59be7a7f52d6044'
AUTHOR_WEB = 'http://ibcomputing.com'

# Services
GOOGLE_ANALYTICS = 'UA-12345678-9'
DISQUS_SITENAME = 'ibcomputing-gitlab-io'

# Social
SOCIAL = (
    ('facebook', 'http://www.facebook.com/mujeebcpy'),
    ('twitter', 'http://twitter.com/mujeebcpy'),
    ('github', 'https://github.com/mujeebcpy'),
    ('gitlab', 'http://www.gitlab.com/mujeebcpy'),
    ('mastodon', 'http://aana.site/mujeebcpy'),
)
# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
