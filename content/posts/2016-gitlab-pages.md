Title: First Post
Date: 2017-10-12
Tags: pelican, gitlab, First post
Slug: pelican-on-gitlab-pages

This site is hosted on GitLab Pages!

The source code of this site is at <https://gitlab.com/pages/pelican>.

Learn about GitLab Pages at <https://pages.gitlab.io>.

My resume <http://mujeebcpy.gitlab.io/resume>.
