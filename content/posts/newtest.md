Title: വീഡിയോ ഫോര്‍മാറ്റുകള്‍
Date: 2018-10-16

# To be Updated
കാണുന്ന extentions container ആണ്.
വീഡിയോകള്‍ വിവിധ കോഡെക്കുകള‍് ഉപയോഗിച്ചാണ് എന്‍കോഡ് ചെയ്തിരിക്കക.
എടുക്കുന്ന വീഡിയോ ലാര്‍ജ് സൈസ്, കംപ്രസ്സ് ചെയ്തിരിക്കും. അത്രയും സൈസ് കുറയും
ഒരുപാട് കോഡെക് ഉണ്ട്. ഇപ്പോഴത്തെ പ്രധാനപ്പെട്ടത് H264 ആണ്.
ഫയല്‍സൈസ് പരമാവധി കുറച്ച് ഇമേജ് ക്വാളിറ്റി കാര്യമായി നഷ്ടപ്പെടുത്താതെ എന്‍കോഡ് ചെ്യുന്നു.
Mpeg-2 (DVD -TV) വിന്റെ അതേ ക്വാളിറ്റിയില്‍ h264 പകുതി സൈസേ വരൂ
പുതിയ കോഡെക്ക് ആണ് h265 more efficient
High Efficiency Video encoding Hevc
പേറ്റന്റ് കാരണം
VP8, Vp9 പുതിയ ഓപണ്‍സോഴ്സ് കോഡെക് ആണ്. h264 ന് സെയിം ആണ് ടെക്നോളജിക്കലി.

#Containers
##MP4
h264 or mpeg എന്‍കോഡഡ് വീഡിയോ , aac, mp3 തുടങ്ങിയ ഓഡിയോയും Also supports subtitle tracks.

##MKV (Matroska Video Container)
all kinds of video and audio codecs, plus multiple subtitle tracks and DVD menus and chapters,

##AVI
video and audio only. less compression, More size

ഏത് തെരഞ്ഞെടുക്കണം.
എല്ലാ ഡിവൈസിലും സപോര്‍ടുള്ളത് mp4
വളര്‍ന്നുകൊണ്ടിരിക്കുന്നത് MKV


