Title: ഗ്നുലിനക്സ് ഇന്‍സ്റ്റാള്‍ ചെയ്യുന്നതിന് മുമ്പായി ശ്രദ്ധിക്കേണ്ട കാര്യങ്ങള്‍
Date: 2017-12-07
Tags: Gnu/Linux, Foss, ubuntu 
Category: GnuLinux
സ്വതന്ത്രസോഫ്റ്റ് വെയറായ ഓപറേറ്റിംഗ് സിസ്റ്റമാണ് ഗ്നുലിനക്സ്. ഉബുണ്ടു, ലിനക്സ് മിന്റ്, ഫെഡോറ എണ്ണിയാലൊടുങ്ങാത്ത നിരവധി പതിപ്പുകള്‍ ഗ്നുലിനക്സിലുണ്ട്. മറ്റ് ഓപറേറ്റിംഗ് സിസ്റ്റത്തെ (windows) നിലനിര്‍ത്തിക്കൊണ്ട് സ്വന്തമായി ഒരു ലിനക്സ് ഡിസ്ട്രോ ഇന്‍സ്റ്റാള്‍ ചെയ്യുമ്പോള്‍ കുറച്ച് കാര്യങ്ങള്‍ ശ്രദ്ധിക്കേണ്ടതുണ്ട്.

* വിന്റോസ് ഏത് ബയോസ് മോഡിലാണോ ഇന്‍സ്റ്റാള്‍ ചെയ്തത് അതില്‍ തന്നെ ഗ്നുലിനക്സും ഇന്‍സ്റ്റാള്‍ ചെയ്യുക
* പാര്‍ടിഷനിംഗ് സ്കീം പരിശോധിക്കുക, Mbr or gpt
* 30-50gb ഉള്ള ഫ്രീസ്പേസ് ഉണ്ടാക്കുക
* സെക്യുര്‍ ബൂട്ട് ഡിസേബിള്‍ ചെയ്യുക

## നിങ്ങളുടെ വിന്റോസ് uefi യിലാണോ അതോ ലെഗസിയിലോ ?
മതര്‍ബോര്‍ഡിന്റെയും ഓപറേറ്റിംഗ് സിസ്റ്റത്തിന്റെയും ഇടയില്‍ പ്രവര്‍ത്തിക്കുന്ന സോഫ്റ്റ് വെയറിനെ പറയുന്ന പേരാണ്  firmwire.  BIOS (Basic input/output Sysytem) എന്ന ഫേംവെയര്‍ അറിയപ്പെടുന്നത് legacy എന്നാണ്.  ഇത് 1975 ല്‍ IBM ആണ് പുറത്തിറക്കിയത്.
എന്നാല്‍ ഇന്നും ഇത് വ്യാപകമായി ഉപയോഗിക്കപ്പെടുന്നുണ്ട്. കാലം മുന്നോട്ട് പോയപ്പോള്‍ ഈ ഫേംവെയറിന് എല്ലാ ഹാര്‍ഡ് വെയറുകളെയും സപ്പോര്‍ട്ട് ചെയ്യാനുള്ള കഴിവ് ഇല്ലാതെയായി.  അപ്പോഴാണ് അവിടേക്ക്  UEFI കടന്നുവരുന്നത്.
**Unified Extensible Firmware Interface(UEFI)**  എന്ന് അറിയപ്പെടുന്ന ഇവന്‍ ബയോസിന്റെ പിന്‍ഗാമിയാണ്.
ഒരു ഓപറേറ്റിംഗ് സിസ്റ്റം ഇന്‍സ്റ്റാള്‍ ചെയ്യുമ്പോള്‍ അത് ലെഗസി മോഡിലോ അല്ലെങ്കില്‍ uefi മോഡിലോ ചെയ്യാവുന്നതാണ്. ബയോസ് സെറ്റിംഗിസില്‍ നിന്നും uefi, legacy ഇവയില്‍ ഒന്ന് മാത്രമോ ഒരേ സമയം രണ്ടും കൂടിയോ ആക്ടിവേറ്റ് ചെയ്ത് വെക്കാനുമാകും.  ആദ്യമായി നിങ്ങളുടെ വിന്റോസ് uefi യില്‍ ആണോ ലെഗസിയില്‍ ആണോ എന്ന് പരിശോധിക്കണം. ഇതിനായി

`Right click Mycomputer -> manage -> disk management`
![manage]({static}/images/manage.png)

Efi എന്ന പേരിലുള്ള  **100 MB** പാര്‍ടിഷന്‍ കാണാന്‍ സാധിച്ചാല്‍ വിന്റോസ്  uefi മോഡിലാണ് ഇന്‍സ്റ്റാള്‍ ചെയ്തിരിക്കുന്നതെന്ന് മനസ്സിലാക്കാം.

![Uefi Bios]({static}/images/UEFI_BIOS.png)

winkey+R അമര്‍ത്തിയശേഷം msinfo32 എന്ന് ടൈപ് ചെയ്തും കണ്ടുപിടിക്കാം.

![msinfo32]({static}/images/msinfo32.png)
![msinfo32]({static}/images/uefi.png)

## ഹാര്‍ഡ് ഡിസ്ക് പാര്‍ടിഷനിംഗ് രീതി MBR ആണോ അതോ GPT യോ ?
ഇത് കണ്ടെത്താന്‍
`Right click Mycomputer -> manage -> disk management` ല്‍ ഹാര്‍ഡ് ഡിസ്ക് ഐക്കണില്‍ റൈറ്റ് ക്ലിക്ക് ചെയ്തശേഷം പ്രോപര്‍ട്ടീസ് അമര്‍ത്തുക,
![newpart]({static}/images/properties.png)
volume എന്ന ടാബില്‍ ഏത് രീതിയാണ് എന്നത് കാണാനാകും.
![mbrgpt]({static}/images/gpt.png)
 MBR ആണെങ്കില്‍ നാല് പ്രൈമറി പാര്‍ട്ടിഷനുകള്‍ മാത്രമാണ് നിര്‍മിക്കാനാവുക. അതുകൊണ്ട് നിലവില്‍ നാല് പ്രൈമറി പാര്‍ട്ടിഷനുകള്‍ ഉണ്ടെങ്കില്‍ പുതിയ പാര്‍ടിഷനില്‍ ഗ്നുലിനക്സ് ഇന്‍സ്റ്റാള്‍ ചെയ്യാനാവില്ല. ഒരു പാര്‍ടിഷന്‍ ഇതിനായി ഡിലീറ്റ് ചെയ്യേണ്ടിവരും. Gpt ആണെങ്കില്‍ 128 പ്രൈമറി പാര്‍ട്ടിഷന്‍ വരെ സാധ്യമാണ്.

## പുതിയ പാര്‍ടിഷന്‍ ഉണ്ടാക്കാം
ഇനി ഒരു 30 മുതല്‍ 50 gb വരെ മിനിമം വലിപ്പത്തില്‍ ഫ്രീസ്പേസ് ഉണ്ടാക്കണം. ഇതിനായി `Right click Mycomputer -> manage -> disk management` ല്‍ കൂടുതല്‍ സ്പേസ് ഉള്ള ഡ്രൈവില്‍ right button ക്ലിക്ക് ചെയ്ത് shrink volume എന്ന് അമര്‍ത്തി ആവശ്യമായ സ്പേസ് MB യില്‍ എഴുതുക. ശേഷം apply ചെയ്യുക.
![newpart]({static}/images/shrinkvolume.jpg)

## സെക്യൂര്‍ ബൂട്ട് ഡിസേബിള്‍ ചെയ്യാന്‍ മറക്കരുത്
ബൂട്ടിംഗ് സമയത്തെ വൈറസിനെ നിലയ്ക്കുനിര്‍ത്താന്‍ കണ്ടെത്തിയ ഒരുമാര്‍ഗമാണ് സെക്യുര്‍ബൂട്ട്. ഇത് എനേബിളായ അവസ്ഥയില്‍ മറ്റൊരു ഓപറേറ്റിംഗ് സിസ്റ്റം ഇന്‍സ്റ്റാള്‍ ചെയ്യാനാവില്ല. ബയോസ് സെറ്റിംഗ്സില്‍ സെക്യൂരിറ്റി ടാബില്‍ പോയി ഇത് ഡിസേബിള്‍ ചെയ്യാം. ചില കമ്പ്യൂട്ടറുകളില്‍ ഈ സെറ്റിംഗ്സ് മാറ്റാന്‍ കഴിയാത്ത അവസ്ഥയിലായിരിക്കും. അത്തരം കമ്പ്യൂട്ടറുകളില്‍ ബയോസ് റൂട്ട് പാസ്‍വേഡ് സെറ്റ് ചെയ്താല്‍ മാത്രമാണ് സെക്യുര്‍ബൂട്ട് ഡിസേബിള്‍ ചെയ്യാന്‍ കഴിയുക.

ഇത്രയും നോക്കി കഴിഞ്ഞാല്‍ നമുക്ക് ഗ്നുലിനക്സ് ഇന്‍സ്റ്റാള്‍ ചെയ്യാന്‍ ആരംഭിക്കാം. നേരത്തെ പരിശോധിച്ചവയില്‍ നിന്ന് വിന്റോസ് uefi ആണെങ്കില്‍ ലിനക്സ് uefi യിലും legeacy യില്‍ ആണെങ്കില്‍ legacy യിലും ഗ്നുലിനക്സ് ഇന്‍സ്റ്റാള്‍ ചെയ്യല്‍ നിര്‍ബന്ധമാണ്. അല്ലെങ്കില്‍ ഇന്‍സ്റ്റാള്‍ ചെയ്തശേഷം വിന്റോസ് കാണാനാകില്ല. (വിന്റോസ് ഡിലീറ്റ് ചെയ്യപ്പെടുന്നില്ല. ബൂട്ട് ലോഡറില്‍ ലഭ്യമാവില്ല. മറ്റൊരു വഴിക്ക് ബൂട്ട് ചെയ്യാനും സാധിക്കും).  
Gpt ആണെങ്കില്‍ പാര്‍ടിഷനിംഗില്‍ ഒന്നും നോക്കാതെ മുന്നേറാം. MBR ആണെങ്കില്‍ 4  പ്രൈമറി പാര്‍ടിഷന്‍ ഇല്ലെന്ന് ഉറപ്പുവരുത്തുക. ഉണ്ടെങ്കില്‍ ഒരു ഡ്രൈവ് ഫ്രീ ആക്കി ഡ‍ിലീറ്റ് ചെയ്യേണ്ടിവരും.

## പുതുതായി ഇന്‍സ്റ്റാള്‍ ചെയ്യുന്ന ഓഎസിന്റെ ബയോസ് മോഡ് തീരുമാനിക്കാം.
Uefi യിലാണെങ്കില്‍ uefi യില്‍ തന്നെ ഇന്‍സ്റ്റാള്‍ ചെയ്യണമെന്ന് സൂചിപ്പിച്ചു.. ഇനി ഇന്‍സ്റ്റാള്‍ ചെയ്യുമ്പോള്‍ അത് ഏത് മോഡിലാണെന്നത് എങ്ങനെ ഉറപ്പിക്കാമെന്ന് നോക്കാം. വിന്റോസ് uefi ആണ് എങ്കില്‍ ബയോസ് സെറ്റിംഗ്സില്‍ പോയി legacy സപ്പോര്‍ട്ട് ഡിസേബിള്‍ ചെയ്തിടുന്നതാണ് നല്ല മാര്‍ഗം. അല്ലെങ്കില്‍ ബൂട്ട് ചെയ്യുന്ന സമയത്ത് തെരഞ്ഞെടുക്കാനുള്ള ഓപ്ഷനുമുണ്ട്. F9, 12 തുടങ്ങിയ (കമ്പ്യൂട്ടറുകള്‍ക്കനുസരിച്ച് മാറ്റം വരും) ബൂട്ട് ഓപ്ഷന്‍ തെരഞ്ഞെടുക്കാനുള്ള കീ പ്രസ് ചെയ്താല്‍ നിങ്ങളുടെ ബൂട്ട് ഡിവൈസുകള്‍ ലിസ്റ്റ് ചെയ്ത് വരും. ഒരു പെന്‍ഡ്രൈവ് ആണ് ഇന്‍സ്റ്റാളിംഗ് മീഡിയ ആയി ഉപയോഗിക്കുന്നതെങ്കില്‍ പെന്‍ഡ്രൈവിന്റെ പേര് രണ്ട് തവണ കാണാന്‍ സാധിക്കും. ഒന്നില്‍ efi എന്ന് കൂടെ ഉണ്ടാകും. അതില്‍ ക്ലിക്ക് ചെയ്ത് ഇന്‍സ്റ്റാളേഷന്‍ തുടങ്ങിയാല്‍ uefi മോഡിലും മറിച്ചാണെങ്കില‍് ലെഗസി മോഡിലും ആണ് ഓഎസ് ഇന്‍സ്റ്റാള്‍ ആവുക.
![choose device]({static}/images/bootmenu.png)
